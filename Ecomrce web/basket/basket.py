class Basket():

    def __init__(self, request):
        print("\n\nrequest session ", request.session)
        self.session = request.session
        print("\n\nself session ", self.session)
        basket = self.session.get('skey')
        basket = self.session.get('skey')
        if 'skey' not in request.session:
            print("inside if ")
            basket = self.session['skey'] = {}
            print("after jason")
        self.basket = basket
        print("return")

    def add(self, product, qty):
        product_id = str(product.id)
        if product_id in self.basket:
            self.basket[product_id]['qty'] = qty
        else:
            self.basket[product_id] = {'price': str(product.price), 'qty': qty}

    def __len__(self):
        """
        Get the basket data and count the qty of items
        """
        # print("\n\nvalueeee", self.basket.values())
        # for i in self.basket.values():
        # print("\n\n\self.baket vaue",i)
        return sum(item['qty'] for item in self.basket.values())

    def save(self):
        self.session.modified = True
