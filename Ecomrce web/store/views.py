from django.shortcuts import render, get_object_or_404
from .models import Product, Category


# Create your views here.

# def categories(request):
#     return {
#         'categories': Category.objects.all()
#     }


def all_products(request):
    products = Product.objects.filter(is_active=True)
    # products = Product.objects.all()
    # for i in products:
    #     print("\n\n\print ", i.tittle)
    return render(request, 'store/home.html', {'products': products})


def category_list(request, anyvariable=None):
    category = get_object_or_404(Category, slug=anyvariable)
    products = Product.objects.filter(category=category)
    return render(request, 'store/products/category.html', {'category': category, 'products': products})


def product_detail(request, tittle_slug):
    # print("\n\n tittle_slug,", tittle_slug)
    product = get_object_or_404(Product, tittle=tittle_slug, in_stock=True)
    return render(request, 'store/products/detail.html', {'product': product})

