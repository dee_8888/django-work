from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls.static import static

app_name = 'store'
urlpatterns = [
    path('', views.all_products, name='all_products'),
    path('cate/<slug:anyvariable>', views.category_list, name='category_list'),
    path('product/<str:tittle_slug>', views.product_detail, name='product_list')
]