# Generated by Django 3.2.6 on 2021-08-13 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patientinfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('f_name', models.CharField(max_length=150)),
                ('l_name', models.CharField(max_length=150)),
                ('un_name', models.CharField(max_length=150)),
                ('pwd', models.CharField(max_length=150)),
            ],
        ),
    ]
