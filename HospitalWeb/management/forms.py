from django import forms
from django.forms import ModelForm
from .models import Patientinfo

class PatientForm(ModelForm):
    class Meta:
        model = Patientinfo
        fields = ('f_name', 'l_name', 'un_name', 'pwd')
        # labels ={
        #     'f_name':'',
        #     'l_name':''
        # }
        widgets = {
            'f_name':forms.TextInput(attrs={'class' :'form-control'}),
            'l_name':forms.TextInput(attrs={'class' :'form-control'}),
            'un_name':forms.TextInput(attrs={'class' :'form-control'}),
            'pwd': forms.TextInput(attrs={'class' :'form-control'}),
        }